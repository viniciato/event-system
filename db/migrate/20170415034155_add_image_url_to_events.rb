class AddImageUrlToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :imageUrl, :string
  end
end
