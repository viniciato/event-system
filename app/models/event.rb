class Event < ApplicationRecord
  belongs_to :user
  has_many :comment, dependent: :destroy
  has_many :active_relationships, class_name:  "Relationship", foreign_key: "event_id", dependent: :destroy
  has_many :users_participating, through: :active_relationships,  source: :user
  validates :name, presence: true
  validates :location, presence: true
  validates :vacancies, presence: true
  validates :description, presence: true
  validates :user_id, presence: true
  validates :date, presence: true
  validates :imageUrl, presence: true
end
