class User < ApplicationRecord
  has_many :events
  has_many :comments, dependent: :destroy
  has_many :active_relationships, class_name:  "Relationship", foreign_key: "user_id", dependent: :destroy
  has_many :events_participating, through: :active_relationships,  source: :event
  def self.sign_in_from_omniauth(auth)
    find_by(provider: auth['provider'], uid: auth['uid']) || create_user_from_omniauth(auth)
  end

  def self.create_user_from_omniauth(auth)
    create(
      provider: auth['provider'],
      uid: auth['uid'],
      id: auth['info']['id'],
      name: auth['info']['name'],
      image: auth['info']['image']
    )
  end

  def post(comment)
    comments << comment
  end

  def attend(event)
      events_participating << event
  end

  def default(event)
    events_participating.delete(event)
  end

  def attending?(event)
    events_participating.include?(event)
  end
end
