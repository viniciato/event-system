class RelationshipsController < ApplicationController
  def create
    @id = params[:relationship][:event_id]
    event = Event.find(@id)
    if event.vacancies > 0
      current_user.attend(event)
      vacancies = event.vacancies-1
      event.update(vacancies: vacancies)
      redirect_to event
    else
      redirect_to event, notice: 'Não temos vagas!'
    end
  end

  def destroy
    event = Relationship.find(params[:id]).event
    current_user.default(event)
    redirect_to event
  end
end
