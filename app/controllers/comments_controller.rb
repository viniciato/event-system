class CommentsController < ApplicationController

  def new
    @comment = Comment.new
  end

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      @event = Event.find(@comment.event_id)
      redirect_to @event, notice: 'Comentario Criado'
    else
      render :new
    end
  end

  def destroy
    event = Relationship.find(params[:id]).event
    current_user.default(event)
    redirect_to event
  end

  private
    def comment_params
      params.require(:comment).permit(:content, :user_id, :event_id)
    end
end
