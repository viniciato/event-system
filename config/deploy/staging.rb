set :stage, :staging

server '104.131.119.113', roles: %w(app web db), primary: true, user: 'deployer'
set :rails_env, "production"
