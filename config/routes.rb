Rails.application.routes.draw do
  resources :events do
    member do
      get :participants
    end
  end
  resources :users
  root 'events#index'
  get 'auth/:provider/callback', to: "sessions#create"
  delete 'sign_out', to: "sessions#destroy", as: "sign_out"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :relationships, only: [:create, :destroy]
  resources :comments
end
