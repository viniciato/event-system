FactoryGirl.define do
  factory :event do
    name "MyString"
    location "MyString"
    date "2017-04-14 19:13:38"
    vacancies 1
    description "MyText"
    user_id 1
  end
end
