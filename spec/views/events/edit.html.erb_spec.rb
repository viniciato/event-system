require 'rails_helper'

RSpec.describe "events/edit", type: :view do
  before(:each) do
    @event = assign(:event, Event.create!(
      :name => "MyString",
      :location => "MyString",
      :vacancies => 1,
      :description => "MyText",
      :user_id => 1
    ))
  end

  it "renders the edit event form" do
    render

    assert_select "form[action=?][method=?]", event_path(@event), "post" do

      assert_select "input#event_name[name=?]", "event[name]"

      assert_select "input#event_location[name=?]", "event[location]"

      assert_select "input#event_vacancies[name=?]", "event[vacancies]"

      assert_select "textarea#event_description[name=?]", "event[description]"

      assert_select "input#event_user_id[name=?]", "event[user_id]"
    end
  end
end
